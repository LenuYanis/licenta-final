const express= require("express");
const bodyParser=require("body-parser");
const sequelize=require("sequelize");
const session = require("client-sessions");
const { Op } = require("sequelize");
const nodemailer=require("nodemailer");
const cors = require("cors");
const { query } = require("express");


const connection=new sequelize("LicentaDB2","root","",{dialect: "mysql"});
let port= 8081;
const app= express();
app.use(function(req,res,next){
  res.header('Access-Control-Allow-Origin','*');
  res.header('Access-Control-Allow-Methods','POST');
  res.header('Access-Control-Alloe-Headers','Content-type');
  next();
})

const configure = app => {
    app.use(cors());
    
    app.use(
      session({
        cookieName: "session",
        secret: "cod secret foarte secret",
        duration: 7200000,
        activeDuration: 300000,
        httpOnly: true,
        ephemeral: true
      })
    
    );

app.use(bodyParser.json());


///////////////////////////////////////////////////////////

const Users=connection.define("users",{
     
    email:sequelize.STRING,
    password:sequelize.STRING,
    token:sequelize.STRING,
    firstName:sequelize.STRING,
    lastName:sequelize.STRING,
    email:sequelize.STRING,
    phoneNumber:sequelize.STRING    
} ,{
    timestamps: false
});

const UsersCompany=connection.define("userscompanies",{
     
  email:sequelize.STRING,
  password:sequelize.STRING,
  Name:sequelize.STRING,
  Company:sequelize.STRING,
   
} ,{
  timestamps: false
});

const CVs=connection.define("cvs",{ 
    university:sequelize.STRING,
    hardSkills:sequelize.STRING,
    languages:sequelize.STRING,
    softSkills:sequelize.STRING,
    workExp1:sequelize.STRING,    
    project1:sequelize.STRING,   
    certificate1:sequelize.STRING,
    
},{
    timestamps:false
});

const HardSkillsDB=connection.define("hardskillsdb",{
  label:sequelize.STRING,
  value:sequelize.STRING,},
  {
    timestamps:false
});

const Interested=connection.define("interesteds",{
  idEmp:sequelize.STRING,
  idStud:sequelize.STRING
},{
  timestamps:false
});



Users.hasOne(CVs);


////////////////////////////////////////////////////////////
  
  
app.post("/login",async(req,res)=>{
    const {email, password}=req.body;
   
 
    const user= await Users.findOne({where:{email, password}, raw:true});
    try{
    if(!user){
        res.status(403).send({message:"Incorect email or password!"});
    }
    else{  
        if (req.session.id) {
            res.status(202).send({ message: "Already logged it" });
          } else {
            req.session.id = user.id;
            req.session.token = user.token;
       
            res.status(200).send({ message: "Successful login"+ " With ID: "+req.session.id , token:req.session.token,id:req.session.id});
           
          }
        }        
    }    
    catch(e){
        console.error(e);
        res.status(500).send({
          message: "Error"
        });
    }
});


app.get("/logout/:id",async (req, res) => {
    const{id}=req.params;
    req.session.reset();
    res.status(200).send({ message: "Successful logout for ID: "+id});
  });

  app.post("/register",async(req,res)=>{
    try{
        const {email,password,lastName,firstName,phoneNumber}= req.body;
        const errors=[];

        if(!email){
            errors.push("Email is empty!");
        }
        if(!password){
            errors.push("Password is Empty!");
        }
        if(!lastName){
          errors.push("Last Name empty!");
          
        }
        if(!firstName){
          errors.push("First Name Empty!")
        }

        if(errors.length==0){
            
            await Users.create({firstName,lastName,email,password,phoneNumber,  token: Math.random().toString(36)});
            res.status(201).send({message: 'User has been created '});
        }
        else{
            res.status(400).send({errors});
        }
    }
    catch(e){
        console.error(e);
  res.status(500).send({
    message: "Error"
  });
 }
});
/////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////
app.post("/CV/:id",async(req,res)=>{
  try{
    const{university,hardSkills,languages,softSkills,workExp1,project1,certificate1}=req.body;   
    const{id}=req.params;
    
    const errors=[]
   
    if(!university){
        errors.push("University field is empty!")
    }
    if(!hardSkills){
        errors.push("HardSkils boxes are not checked")
    }

    if(errors.length==0){
        await CVs.create({id,university,hardSkills,languages,softSkills,workExp1,project1,certificate1,id});
        res.status(201).send({message: "CV added good job! Id= "+id});
    }
    else{
        res.status(400).send({errors});
    }

    }
    catch(e){
        console.error(e);
        res.status(500).send({message:e});
    }

})

app.get("/CV/:id", async (req, res) => {
    try {
      const CVuri = await CVs.findAll({
        where: { id: req.params.id },
        raw: true
      });
      res.status(200).send(CVuri);
    } catch (e) {
      console.error(e);
      res.status(500).send({ message: "server error at Get" });
    }
  });

app.put("/CV/:id",async(req,res)=>{
    try{
       const {id}=req.params
        const{university,hardSkills,languages,softSkills,workExp1,project1,certificate1}=req.body;
        const cv=await CVs.findAll({
          where: { id },
          raw: true
        })
        if(!cv){
          res.status(400).send({ message: "CV does not exist" });
        }
        else{
            const updateCv=await CVs.update({
              ...cv,
              id,
              university,
              hardSkills,
              languages,
              softSkills,
              workExp1,
              project1,
              certificate1,
              },{where:{id}});
          res.status(200).send({ updateCv, message: "CV has been updated" });
        }

    }catch (e) {
      console.error(e);
      res.status(500).send({ message: "server error at Update" });
    }
  })
 //////////////////////////////////////////////////////////////////////////////////



  app.get("/profilePage/:id",  async (req, res) => {
    try {
      const profile = await Users.findAll({
        where: { id: req.params.id },
        raw: true
      });
      res.status(200).send(profile);
    } catch (e) {
      console.error(e);
      res.status(500).send({ message: "server error" });
    }
  });

///////////////////////////////////////////////////////////////////////////////////////
  app.post("/loginCompany",async(req,res)=>{
    const {email, password}=req.body;
   
  
    const user= await UsersCompany.findOne({where:{email, password}, raw:true});
    try{
    if(!user){
        res.status(403).send({message:"Incorect email or password!"});
    }
    else{           
            res.status(200).send({message:"Succseful!", user})         
        }       
    }    
    catch(e){
        console.error(e);
        res.status(500).send({
          message: "Error"
        });
    }
  });

///////////////////////////////////////////////////////////
app.post("/searchCv", async (req, res) => {
  try {
    const{university,hardSkills,languages,softSkills}=req.body;
    let query={};
    if (university){
      query.university=university;
    }

    if(hardSkills){
      query.hardSkills=hardSkills
    }

    if(languages){
      query.languages=languages;
    }

    if(softSkills){
      query.softSkills=softSkills;
    }
    
    
    const CVuri = await CVs.findAll({where:{
    [Op.and]:{
      hardSkills:{[Op.substring]:query.hardSkills},
      university:{[Op.eq]:query.university},
      languages:{[Op.substring]:query.languages}
     
    }},raw:true});
    res.status(200).send(CVuri);
  } catch (e) {
    console.error(e);
    res.status(500).send({ message: "server error" });
  }
});

app.post("/ListCv", async (req, res) => {
  try {
      const {id}=req.body;
      var array = JSON.parse("[" + id + "]");
      var arrayUseri=[];
    for(var i=0;i<array.length;i++){

    var profile =  await Users.findAll({
      where: {id:array[i]} });
    arrayUseri.push(profile);
  }
   res.status(200).send(arrayUseri);
} catch (e) {
    console.error(e);
    res.status(500).send({ message: "server error" });
  }
});

app.post("/SendEmail",async(req,res)=>{
  try{
     const{email,emailText,vectorStudenti,id}=req.body;
     var idStud = vectorStudenti.toString();
     const idAngajat=await Interested.findOne({
      where: { id },
      raw: true
    })
    if(idAngajat){
     const updateInterested=await Interested.update({
      ...idAngajat,
      idStud
      
      },{where:{id}});
      res.status(200).send({message:"Updated for Interested"});
    }

    //  var transport=nodemailer.createTransport({
    //    service:'gmail',
    //    auth:{
    //      user:'lenuyanis@gmail.com',
    //      pass:'asdfghjklasdfghjkl'
    //    }
    //  });

    //  var mailFunctions={
    //    from:'me@gmail.com',
    //    to:email,
    //    subject:'Someone wants you!',
    //    text:emailText
    //  }

    //  transport.sendMail(mailFunctions,function(error,info){
    //    if(error){
    //      console.log(error);
    //    }
    //    else{
    //      console.log("Email sent to: "+arrayEmail);
    //    }
    //  })


    }
  catch(e){
    console.error(e);
    res.status(500).send({ message: "Email Error" });
  }
})

////////////////////////////////////////////////////////
app.post('/getHS',async(req,res)=>{
  try{
    var hardSkillsFromDb= await HardSkillsDB.findAll();
    res.status(200).send(hardSkillsFromDb);
  }
  catch(e){
    console.error(e);
    res.status(500).send({ message: "DB Error" });
  }
})

app.get("/getInterestedStud/:id",async(req,res)=>{
  try{
    var interestedStud=await Interested.findOne({
      where: { id: req.params.id},
      raw: true
    });

    var arrayId=JSON.parse("[" + interestedStud.idStud + "]")
    var student=[];
    for(var i=0;i<arrayId.length;i++){
    student.push(await Users.findAll({ where: { id: arrayId[i] },
      raw: true}));
    }
    res.status(200).send(student);
  }
  catch(e){
    console.error(e);
    res.status(500).send({ message: "Interested Error" });
  }
})


app.get("/seeInterestedEmp/:id",async(req,res)=>{
  try{
  var idul=""+req.params.id;
  let arrayAngajatori=[];
  var interestedEmp= await Interested.findAll({where:{
      idStud:{
        [Op.substring]:req.params.id}
      }});
  
  for(var i=0;i<interestedEmp.length;i++){
    arrayAngajatori.push(await UsersCompany.findOne({where:{id:interestedEmp[i].id}}))
  }

  res.status(200).send(arrayAngajatori);
  
}
catch(e){
  console.error(e);
  res.status(500).send({ message: "Interested Error" });
}
  
})

};
///////////////////////////////////////////////////////




///////////////////////////////////////////////////////////////
module.exports = configure;

configure(app);
app.listen(port, () => {
    console.log("Serverul merge pe " + port);
  });

