import axios from 'axios'


export  function findUser({ commit },data) {
    const response=axios.post("http://localhost:8081/login",data)
                        .then(response=>{   
                            commit("SET_USER", response.data);
                            commit("SET_TOKEN",response.data.token);
                            sessionStorage.setItem('token',response.data.token);
                            sessionStorage.setItem('id',response.data.id);
                            sessionStorage.setItem('firstName',response.data.FirstName);
                            sessionStorage.setItem('lastName',response.data.LastName); 
                            sessionStorage.setItem('email',response.data.email);
                            sessionStorage.setItem('phoneNumber',response.data.PhoneNumber); 
                            sessionStorage.setItem('reload',false);
                            console.log(sessionStorage);
                            console.log(response);
                            this.$router.push("/profilePage");})
                        .catch(error => {
                            console.log(error)        
                        });
  }

export function logOut({commit}){
    const response=axios.get("http://localhost:8081/logout/"+sessionStorage.getItem('id'))
                        .then(response=>{
                            console.log(response);
                            commit("SET_LOGOUT");
                            sessionStorage.clear();
                            this.$router.push("/login");})
                        .catch(error => {
                            console.log(error)        
                        });
}

export function logOutCompany({commit}){
    const response=axios.get("http://localhost:8081/logout/"+sessionStorage.getItem('id'))
                        .then(response=>{
                            console.log(response);
                            
                            sessionStorage.clear();
                            this.$router.push("/loginCompany");})
                        .catch(error => {
                            console.log(error)        
                        });
}



export function getUserProfileData({commit}){
    const response=axios.get("http://localhost:8081/profilePage/"+sessionStorage.getItem('id'))
                        .then(response=>{
                            commit("SET_PROFILEDATA",response.data);
                            console.log(response.data[0]);
                            sessionStorage.setItem('firstName',response.data[0].firstName);
                            sessionStorage.setItem('lastName',response.data[0].lastName); 
                            sessionStorage.setItem('email',response.data[0].email);
                            sessionStorage.setItem('phoneNumber',response.data[0].phoneNumber);           
                            })
                        .catch(error => {
                            console.log(error)        
                        });
   
}

export function postCVintoDB({commit},data){
    const response=axios.post("http://localhost:8081/CV/"+sessionStorage.getItem("id"),data)
                        .then(response=>{
                            commit("SET_CV",response.data);
                            console.log(response);
                        })
                        .catch(error => {
                            console.log(error)        
                        });
}

export function getUserCV({commit}){
    const response=axios.get("http://localhost:8081/CV/"+sessionStorage.getItem("id"))
                        .then(response=>{
                            commit("SET_CV",response.data);
                            console.log(response.data);
                            sessionStorage.setItem("University",response.data[0].university);
                            sessionStorage.setItem("HardSkills",response.data[0].hardSkills);
                            sessionStorage.setItem("Languages",response.data[0].languages);
                            sessionStorage.setItem("SoftSkills",response.data[0].softSkills);
                            sessionStorage.setItem("WorkExp",response.data[0].workExp1);
                            sessionStorage.setItem("PersonalProjects",response.data[0].project1);
                            sessionStorage.setItem("Certificates",response.data[0].certificate1);
                            
                        })
                        .catch(error => {
                            console.log(error)        
                        });
}

export function findUserCompany({commit},data){
    const response= axios.post("http://localhost:8081/loginCompany",data)
                         .then(response=>{
                            commit("SET_USERCOMPANY", response.data);
                            sessionStorage.setItem('id',response.data.user.id);
                            sessionStorage.setItem('nume',response.data.user.Name);
                            sessionStorage.setItem('numeCompanie',response.data.user.Company);
                            console.log(response.data);
                            sessionStorage.setItem('reload',false);
                            this.$router.push("/CompanyPage");
                         })
                        .catch(error => {
                            console.log(error.response)        
                        });
}

