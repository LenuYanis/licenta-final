export function SET_USER(state, userCred) {
    state.userCred = userCred;
  }
  export function SET_TOKEN(state, token) {
    state.token = token;
  }

export function SET_LOGOUT(state) {
    state.userCred = [];
    state.token="";
    state.profileData=[];
  }

export function SET_PROFILEDATA(state,profileData){
    state.profileData=profileData;   
}

export function SET_CV(state,CV){
     state.CV=CV;
}

export function SET_COMPANY(state, userCompany) {
  state.userCompany = userCompany;
}

export function SET_CVSEARCHED(state,searchedCV){
  state.searchedCV=searchedCV;
}


