import Axios from 'axios';

Axios.defaults.baseURL="http://localhost:8081/login";
const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '/login', component: () => import('pages/LogIn.vue')},
      { path: '/register',component: () => import('pages/Register.vue') },
      { path: '/loginCompany',component: () => import('pages/LogInCompany.vue')}
      
    
      
    ]
  },

  {
    path:'/',
    component:()=>import('layouts/ProfileLayout.vue'),
    children:[
      {path:'/profilePage',component:()=>import("pages/ProfilePage"),meta: { 
        requiresAuth: true
      }},
      {path:'/CV',component:()=>import("pages/CV")},
      {path:'/interested',component:()=>import("pages/Interested")}
    ]
  },

  {
    path: '/',
    component: () => import('layouts/CompanyLayout.vue'),
    children: [
      { path: '/companyPage', component: () => import('pages/CompanyPage.vue')},
      { path: '/CVinterested', component: () => import('pages/CVinterested.vue')},
      { path: '/ListCv', component: () => import('pages/ListCv.vue')},
      {path: '/SingleCvCompany/:id',component: () => import('pages/SingleCvCompany.vue')},
     
    ]}

];

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
